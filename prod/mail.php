<?php

    session_start();

    require_once 'PHPMailer/class.phpmailer.php';

    $sending_error = "";
    $response = "";
    $mailDelay = 60;

    $nameFrom = $_POST['name'];
    $mailFrom = $_POST['email'];
    $mailPhone = $_POST['phone'];

    $subject = "Wiadomość z portalu K&P";
    $mailTo = "krystian.std@gmail.com";
    $message .= "Numer nadawcy: " . $mailPhone . "<br>";
    $message .= "Treść wiadomości:";
    $message .= $_POST['msg'];


    $send_email = new PHPMailer();
    $send_email->From = $mailFrom;
    $send_email->FromName = $nameFrom;
    $send_email->Subject = $subject;
    $send_email->Body = $message;
    $send_email->AddAddress($mailTo);
    $send_email->isHTML(true);
    $send_email->CharSet = "UTF-8";

    if (!$send_email->Send()) {
        $sending_error = $send_email->ErrorInfo;
    } else {
        $_SESSION['mailDelay'] = time();
        $sending_error .= "Twoja wiadomość została wysłana.";
    }