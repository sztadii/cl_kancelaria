var accordion;

accordion = new function () {

  //catch DOM
  var $el;
  var $itemTop;
  var $itemBody;

  //bind events
  $(document).ready(function () {
    init();
    makeAccordion();
  });

  //private functions
  var init = function () {
    $el = $(".accordion");
    $itemTop = $('.accordion__top');
    $itemBody = $('.accordion__body');
  };

  var makeAccordion = function () {

    $itemTop.first().addClass('-active');
    $itemBody.first().addClass('-active');

    $itemTop.on('click', function () {
      var $this = $(this);
      var $target = $($this.data('target'));

      $this.siblings().removeClass('-active');
      $target.siblings().removeClass('-active');

      $this.addClass('-active');
      $target.addClass('-active');
    });
  };
};