var banner;

banner = new function () {

  //catch DOM
  var $el;
  var $slider;
  var $sliderHeader;

  //bind events
  $(document).ready(function () {
    init();
  });

  $(window).scroll(function () {
    animation.fading('.banner .container');
  });

  //private functions
  var init = function () {
    $el = $('.banner');
    $slider = $el.find('.banner__slider');
    $sliderHeader = $slider.find('.banner__header');

    if ($slider.length > 0) {
      $slider.imagesLoaded({background: true}).always(function () {

        $slider.slick({
          infinite: true,
          dots: false,
          arrows: true,
          appendArrows: $('.banner__arrows .container'),
          prevArrow: '<div class="banner__arrow -prev"><i class="fa fa-angle-left"></i></div>',
          nextArrow: '<div class="banner__arrow -next"><i class="fa fa-angle-right"></i></div>',
          slidesToShow: 1,
          slidesToScroll: 1,
          fade: true,
          adaptiveHeight: true,
          autoplay: true,
          speed: 800,
          autoplaySpeed: 4000,
          draggable: false,
          pauseOnHover: false,
          pauseOnFocus: false
        });

        $slider.on('beforeChange', function () {
          $sliderHeader.addClass('-hide');
        });

        $slider.on('afterChange', function () {
          $sliderHeader.removeClass('-hide');
          $('.banner__item').addClass('-showing');
        });
      });
    }
  };
};
