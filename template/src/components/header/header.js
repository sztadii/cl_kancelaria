var header;

header = new function () {

  //private functions
  var windowTop = 0;
  var windowWidth;
  var breakpoint = 1024;

  //catch DOM
  var $el;
  var $headerNav;
  var $headerNavOffset;

  //bind events
  $(document).ready(function () {
    init();
  });

  $(window).resize(function () {
    varsUpdate();

    fixStart();
  });

  $(window).scroll(function () {
    setTimeout(function () {
      fixStart();
    },0)
  });

  //private functions
  var init = function () {
    domCatch();
    varsUpdate();

    fixStart();
  };

  var domCatch = function () {
    $el = $('.header');
    $headerNav = $('.header__nav');
  };

  var varsUpdate = function () {
    $headerNavOffset = $headerNav.offset().top;
    windowWidth = $(window).outerWidth();
  };

  var fixStart = function () {
    if(windowWidth >= breakpoint) {
      fixMake();
    } else {
      fixReset();
    }
  };

  var fixMake = function () {
    windowTop = $(window).scrollTop();

    if (windowTop >= $headerNavOffset) {
      fixAdd();
    } else {
      fixReset();
    }
  };

  var fixAdd = function () {
    if(!$el.hasClass('-fixed'))
      $el.addClass('-fixed');
  };

  var fixReset = function () {
    if($el.hasClass('-fixed'))
      $el.removeClass('-fixed');
  };
};